//
//  ViewController.m
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import "ViewController.h"
#import "TexturedView+Protected.h"
#import "TargetFieldView.h"
#import "BallView.h"
#import "ShootView.h"
#import "BlinkView.h"


#define BALL_SIZE               ( 90.f )
#define BLINK_INTERVAL          ( .2f )
#define ANIMATION_STEP_INTERVAL ( .1f)

@interface ViewController () <ShootViewDelegate>
@property (nonatomic, retain, readwrite) TexturedView* view;
@property (nonatomic, retain, readwrite) NSTimer* blinkTimer;
@property (nonatomic, retain, readwrite) NSDate* startShootDate;
@property (nonatomic, assign, readwrite) CGPoint startShootPoint;
- (void)blink:(NSTimer*)theTimer;
- (void)shoot;
@property (nonatomic, retain, readwrite) NSNumber* speed; // speed contain CGFloat
@property (nonatomic, retain, readwrite) NSNumber* angle; // angle contain CGFloat
@property (nonatomic, retain, readwrite) NSTimer* shootTimer;
- (void)step:(NSTimer*)theTimer;
@end


@implementation ViewController

@synthesize targetFieldView = _targetFieldView;
@synthesize ballView        = _ballView;
@synthesize shootView       = _shootView;
@synthesize blinkTimer      = _blinkTimer;
@synthesize startShootDate  = _startShootDate;
@synthesize startShootPoint = _startShootPoint;
@synthesize speed           = _speed;
@synthesize angle           = _angle;
@synthesize shootTimer      = _shootTimer;
@dynamic view;

- (void)dealloc {
    self.shootView.delegate = nil;
    self.targetFieldView = nil;
    self.ballView        = nil;
    self.shootView       = nil;
    self.blinkTimer      = nil;
    self.startShootDate  = nil;
    self.speed           = nil;
    self.angle           = nil;
    self.shootTimer      = nil;
    [super dealloc];
}

- (id)initWithNibName: (NSString*) nibNameOrNil
               bundle: (NSBundle*) nibBundleOrNil
{
    self = [super initWithNibName: nibNameOrNil
                           bundle: nibBundleOrNil];
    if (self) {
        // init
    }
    return self;
}

- (TexturedView *)view
{
    return (TexturedView*)super.view;
}

- (void)blink:(NSTimer*)theTimer
{
    @autoreleasepool
    {
        @synchronized (self)
        {
            [self.targetFieldView reaccentuated];
        }
    }
}

- (void)shoot
{
    [[NSRunLoop currentRunLoop] addTimer: self.shootTimer
                                 forMode: NSDefaultRunLoopMode];
}

- (void)step:(NSTimer*)theTimer
{
    @autoreleasepool
    {
        @synchronized (self)
        {
            NSLog(@">>>");
            if (! self.speed
                ||
                ! self.angle
                ) {
                return;
            }
            CGFloat track   = ANIMATION_STEP_INTERVAL * [self.speed floatValue];
            CGFloat anAngle = [self.angle floatValue];
            if (NAN == anAngle) {
                return;
            }
            // eval next center point
            CGFloat deltaX  = track * sinf( anAngle );
            CGFloat deltaY  = track * cosf( anAngle );
            
            CGFloat newX = self.ballView.center.x - deltaX;
            CGFloat newY = self.ballView.center.y + deltaY;
            NSLog(@"point %@ %@", NSStringFromCGPoint(self.ballView.center), NSStringFromCGPoint(CGPointMake(newX, newY)));
            
            // check wall or targets here
            CGFloat minDistance = NAN; // min distance to target center from closest point or next center
            CGFloat aDistance   = NAN; // from current center to target border
            BlinkView* endTarget = nil;
            
            CGFloat wallDistance = NAN; // distance to wall from next center
            CGPoint wallPoint = CGPointZero; // next current center at wall border
            
            // check left wall
            if (newX < .5f * BALL_SIZE) {
                CGPoint crossPoint = CGPointMake(0.f,
                                                 newY - newX * cosf(anAngle)/sinf(anAngle));
                
                CGFloat wallX = crossPoint.x - newX;
                CGFloat wallY = crossPoint.y - newY;
                CGFloat wallDist = sqrtf(wallX * wallX + wallY * wallY);
                if (isnan(wallDistance)
                    ||
                    wallDistance > wallDist
                    ) {
                    wallDistance = wallDist;

                    CGFloat wallPointX = .5f * BALL_SIZE;
                    CGFloat wallPointY = newY - (.5f * BALL_SIZE) * cosf(anAngle)/sinf(anAngle);
                    wallPoint = CGPointMake(wallPointX, wallPointY);
                }
            }
            
            // check right wall
            if (newX > self.view.frame.size.width - .5f * BALL_SIZE) {
                CGPoint crossPoint = CGPointMake(0.f,
                                                 newY - newX * cosf(anAngle)/sinf(anAngle));
                
                CGFloat wallX = crossPoint.x - newX;
                CGFloat wallY = crossPoint.y - newY;
                CGFloat wallDist = sqrtf(wallX * wallX + wallY * wallY);
                if (isnan(wallDistance)
                    ||
                    wallDistance > wallDist
                    ) {
                    wallDistance = wallDist;

                    CGFloat wallPointX = self.view.frame.size.width - .5f * BALL_SIZE;
                    CGFloat wallPointY = newY - (.5f * BALL_SIZE) * cosf(anAngle)/sinf(anAngle);
                    wallPoint = CGPointMake(wallPointX, wallPointY);
                }
            }
            
            // check targets
            for (BlinkView* target in self.targetFieldView.targets) {
                CGPoint targetCenter = [self.targetFieldView convertPoint: target.center toView: self.view];
                CGFloat dX = targetCenter.x - self.ballView.center.x;
                CGFloat dY = targetCenter.y - self.ballView.center.y;
                
                // between current center and target center
                CGFloat distance = sqrtf(dX * dX + dY * dY);
                
                // eval newToTargetDistance
                CGFloat newToTargetDeltaX = targetCenter.x - newX;
                CGFloat newToTargetDeltaY = targetCenter.y - newY;
                
                CGFloat newToTargetDistance = sqrtf( newToTargetDeltaX * newToTargetDeltaX
                                                    + newToTargetDeltaY * newToTargetDeltaY );

                // eval
                CGFloat a = ABS(track);
                CGFloat b = distance;
                CGFloat c = newToTargetDistance;
                CGFloat p = .5f * (a + b + c);
                CGFloat closestToTargetDistance = 2 * sqrtf(p * (p - a) * (p - b) * (p - c)) / a;

                // point between current and next centers closest to target
                CGFloat currenToClosestDistance = sqrtf(distance * distance
                                                        - closestToTargetDistance * closestToTargetDistance);

                NSLog(@"track: %f   currenToClosestDistance: %f", track, currenToClosestDistance);
                
                // min distance to target center from closest point or next center
                CGFloat minToTargetDistance = newToTargetDistance;
                if (currenToClosestDistance < ABS(track)) {
                    minToTargetDistance = closestToTargetDistance;
                }
                
                
                NSLog(@"%f    %f", minToTargetDistance, distance);
                
                CGFloat realDistance = .5f * (target.frame.size.width + BALL_SIZE);
                if (minToTargetDistance <= realDistance
                    &&
                    (isnan(minDistance)
                     ||
                     minDistance > minToTargetDistance)
                    &&
                    (isnan(wallDistance)
                     ||
                     wallDistance > minToTargetDistance)
                    ) {
                    endTarget = target;
                    minDistance = minToTargetDistance;
                    aDistance = realDistance - distance;
                }
            }
            
            if (endTarget) {
                [self.shootTimer invalidate];
                CGFloat deltaDist = aDistance;
                CGFloat deltaDistX  = deltaDist * sinf( anAngle );
                CGFloat deltaDistY  = deltaDist * cosf( anAngle );
                [UIView animateWithDuration: ABS( ANIMATION_STEP_INTERVAL * deltaDist / track )
                                 animations: ^() {
                                     self.ballView.center = CGPointMake(self.ballView.center.x - deltaDistX,
                                                                        self.ballView.center.y + deltaDistY);
                                 }
                                 completion: ^(BOOL finished) {
                                     NSLog(@"%i", [self.targetFieldView.targets indexOfObject: endTarget]);
                                     [self.blinkTimer invalidate];
                                     [self.targetFieldView hit:
                                      [self.targetFieldView.targets indexOfObject: endTarget]];
                                     [UIView animateWithDuration: .2f
                                                      animations: ^() {
                                                          self.ballView.alpha = 0.f;
                                                      }];
                                 }];
            } else {
                if (! isnan(wallDistance)) { // if wall
                    CGFloat wallDistX = wallPoint.x;
                    CGFloat wallDistY = wallPoint.y;
                    CGFloat deltaDist  = sqrtf( wallDistX * wallDistX + wallDistY * wallDistY);
                    
                    self.angle = [NSNumber numberWithFloat: 2.f * M_PI - [self.angle floatValue]];

                    [UIView animateWithDuration: ABS( ANIMATION_STEP_INTERVAL * deltaDist / track )
                                     animations: ^() {
                                         self.ballView.center = wallPoint;
                                     }];
                } else {
                    [UIView animateWithDuration: /*0.f*/ANIMATION_STEP_INTERVAL
                                     animations: ^() {
                                         self.ballView.center = CGPointMake(newX,
                                                                            newY);
                                     }];
                }
            }
            NSLog(@"<<");
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view = [[[TexturedView alloc] initWithFrame: self.view.frame] autorelease];
    self.view.userInteractionEnabled = YES;
    self.view.image.image = [UIImage imageNamed: @"background.png"];
    
    self.targetFieldView = [[[TargetFieldView alloc] initWithFrame: CGRectMake(0.f,
                                                                               .125f * self.view.frame.size.height,
                                                                               self.view.frame.size.width,
                                                                               .25f * self.view.frame.size.height)]
                            autorelease];
    self.shootView       = [[[ShootView alloc] initWithFrame: CGRectMake(0.f,
                                                                         .75f * self.view.frame.size.height,
                                                                         self.view.frame.size.width,
                                                                         .25f * self.view.frame.size.height)]
                            autorelease];    
    self.ballView        = [[[BallView alloc] initWithFrame: CGRectMake(.5f * (self.shootView.frame.size.width
                                                                               - BALL_SIZE),
                                                                        self.view.frame.size.height - BALL_SIZE,
                                                                        BALL_SIZE,
                                                                        BALL_SIZE)]
                            autorelease];
    self.shootView.delegate = self;
    [self.view addSubview: self.targetFieldView];
    [self.view addSubview: self.shootView];
    [self.view addSubview: self.ballView];
    
    self.blinkTimer = [NSTimer timerWithTimeInterval: BLINK_INTERVAL
                                              target: self
                                            selector: @selector(blink:)
                                            userInfo: nil
                                             repeats: YES];
    self.shootTimer = [NSTimer timerWithTimeInterval: ANIMATION_STEP_INTERVAL
                                              target: self
                                            selector: @selector(step:)
                                            userInfo: nil
                                             repeats: YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.view = nil;

    [self.targetFieldView removeFromSuperview];
    [self.ballView removeFromSuperview];
    [self.shootView removeFromSuperview];
    
    self.targetFieldView = nil;
    self.ballView        = nil;
    self.shootView       = nil;
    
    self.blinkTimer      = nil;
    self.startShootDate  = nil;
    self.speed           = nil;
    self.angle           = nil;
    self.shootTimer      = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.startShootPoint = CGPointZero;
    self.speed           = nil;
    self.angle           = nil;
    self.view.backgroundColor = [UIColor clearColor];
    self.view.alpha = 0.f;
    [self blink: nil];
    [[NSRunLoop currentRunLoop] addTimer: self.blinkTimer
                                 forMode: NSDefaultRunLoopMode];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [UIView animateWithDuration: .5f
                     animations: ^() {
                         self.view.alpha = 1.f;
                     }];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [UIView animateWithDuration: .5f
                     animations: ^() {
                         self.view.alpha = 0.f;
                     }];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
    [self.blinkTimer invalidate];
    [self.shootTimer invalidate];
    self.startShootDate  = nil;
    self.speed           = nil;
    self.angle           = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationPortrait == interfaceOrientation;
}

#pragma mark - ShootViewDelegate

- (void)willShoot:(NSValue*)point
{
    if (! point) {
        return;
    }
    self.startShootDate = [NSDate date];
    self.startShootPoint = [point CGPointValue];
}

- (void)didShoot:(NSValue*)point
{
    if (! self.startShootDate
        ||
        ! point
        ||
        CGPointEqualToPoint(self.startShootPoint, CGPointZero)
        ) {
        return;
    }
    // track eval:
    CGPoint newPoint = [point CGPointValue];
    CGFloat trackHeight = newPoint.y - self.startShootPoint.y;
    CGFloat trackWidth  = newPoint.x - self.startShootPoint.x;
    CGFloat trackHeight2 = trackHeight * trackHeight;
    CGFloat trackWidth2  = trackWidth  * trackWidth;
    CGFloat track = sqrtf(trackHeight2 + trackWidth2);
    if (track < .01f) {
        return;
    }
    // track speed coefficient
    CGFloat trackRatio  = 1.8f * powf(ABS( trackHeight / self.shootView.frame.size.height ), .03125f);
    // track angle
    CGFloat angleSin = trackWidth / track;
    // time interval
    CGFloat shootInterval = [self.startShootDate timeIntervalSinceNow];
    //  result speed
    CGFloat velocity = trackRatio * track  / shootInterval;
    // reset temp vars
    self.startShootDate  = nil;
    self.startShootPoint = CGPointZero;
    
    self.angle = [NSNumber numberWithFloat: asinf(angleSin)];
    self.speed = [NSNumber numberWithFloat: - MIN( ABS(velocity), 3000) ];
    NSLog(@"SPEED: %f", velocity);
    [self shoot];
}

@end
