//
//  ViewController.h
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import <UIKit/UIKit.h>


@class TargetFieldView;
@class BallView;
@class ShootView;

@interface ViewController : UIViewController
@property (nonatomic, retain, readwrite) TargetFieldView* targetFieldView;
@property (nonatomic, retain, readwrite) BallView* ballView;
@property (nonatomic, retain, readwrite) ShootView* shootView;
@end
