//
//  TexturedView+Protected.h
//  shootball
//
//  Created by Pitometsu on 9/5/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TexturedView.h"


@interface TexturedView ()
@property (nonatomic, retain, readwrite) UIImageView* image;
@end
