//
//  main.m
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
