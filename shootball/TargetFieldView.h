//
//  TargetFieldView.h
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TargetFieldView : UIView
@property (nonatomic, retain, readonly) NSArray* targets;
- (void)reaccentuated;
- (void)selectAccentuated:(NSUInteger)index;
- (void)hit:(NSUInteger)index;
@end
