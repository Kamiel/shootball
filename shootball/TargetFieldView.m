//
//  TargetFieldView.m
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import "TargetFieldView.h"
#import "BlinkView.h"


#define BLINK_VIEW_SIZE ( 145.f )
#define TARGETS_COUNT   ( 4 )

@interface TargetFieldView ()
@property (nonatomic, retain, readwrite) NSMutableArray* aTargets;
- (void)fillTargets;
@end


@implementation TargetFieldView

@synthesize aTargets = _aTargets;

- (void)dealloc
{
    self.aTargets = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.aTargets = [NSMutableArray arrayWithCapacity: 4];
        self.backgroundColor = [UIColor clearColor];
        [self fillTargets];
        CGFloat blinkOffset = frame.size.width / TARGETS_COUNT;
        for (BlinkView* target in self.targets) {
            target.center = CGPointMake(([self.targets indexOfObject: target] + .5f) * blinkOffset, .5f * frame.size.height);
            [self addSubview: target];
        }
    }
    return self;
}

- (void)fillTargets
{
    CGRect blinkRect = CGRectMake(0.f, 0.f, BLINK_VIEW_SIZE, BLINK_VIEW_SIZE);
    for (int i = 0; i < TARGETS_COUNT; i++) {
        BlinkView* target = [[BlinkView alloc] initWithFrame: blinkRect];
        target.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin
        | UIViewAutoresizingFlexibleTopMargin
        | UIViewAutoresizingFlexibleRightMargin
        | UIViewAutoresizingFlexibleLeftMargin;
        [self.aTargets addObject: target];
        [target release];
    }
}

- (NSArray *)targets
{
    return (NSArray*)self.aTargets;
}

- (void)reaccentuated
{
    NSUInteger accentuated = arc4random_uniform( self.targets.count );
    [self selectAccentuated: accentuated];
}

- (void)selectAccentuated:(NSUInteger)index
{
    if (index < self.targets.count) {
        for (BlinkView* target in self.targets) {
            target.accentuated = index == [self.targets indexOfObject: target];
        }
    }
}

- (void)hit:(NSUInteger)index
{
    if (index < self.targets.count) {
        for (BlinkView* target in self.targets) {
            if (index == [self.targets indexOfObject: target]) {
                [target hit];
            } else {
                target.accentuated = NO;
            }
        }
    }
}

@end
