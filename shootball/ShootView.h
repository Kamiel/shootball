//
//  ShootView.h
//  shootball
//
//  Created by Pitometsu on 9/5/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TexturedView.h"


@protocol ShootViewDelegate;

@interface ShootView : TexturedView
@property (nonatomic, assign, readwrite) id<ShootViewDelegate> delegate;
@end


@protocol ShootViewDelegate <NSObject>
@optional
- (void)willShoot:(NSValue*)point; // point contain CGPoint
- (void)didShoot:(NSValue*)point; // point contain CGPoint
@end
