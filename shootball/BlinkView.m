//
//  BlinkView.m
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import "BlinkView.h"
#import "TexturedView+Protected.h"


#define ACCENTUATED_IMG ( @"blink_circle.png" )
#define DEFAULT_IMG     ( @"circle.png" )
#define HIT_IMG         ( @"circle_hit.png" )


@implementation BlinkView

@synthesize accentuated = _accentuated;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self->_accentuated = NO;
        self.image.image = [UIImage imageNamed: DEFAULT_IMG];
    }
    return self;
}

- (void)setAccentuated:(BOOL)accentuated
{
    if (self.accentuated != accentuated) {
        self->_accentuated = accentuated;
        self.image.image = self.accentuated ?
        [UIImage imageNamed: ACCENTUATED_IMG] : [UIImage imageNamed: DEFAULT_IMG];
    }
}

- (void)hit
{
    self->_accentuated = YES;
    self.image.image = [UIImage imageNamed: HIT_IMG];
}

@end
