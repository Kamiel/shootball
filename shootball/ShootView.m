//
//  ShootView.m
//  shootball
//
//  Created by Pitometsu on 9/5/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import "ShootView.h"
#import "TexturedView+Protected.h"


@implementation ShootView

@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.image.image = [UIImage imageNamed: @"shoot_field.png"];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.delegate
        &&
        [self.delegate respondsToSelector: @selector(willShoot:)]
        ) {
        UITouch* touch = [touches anyObject];
        CGPoint location = [touch locationInView: self];
        [self.delegate willShoot: [NSValue valueWithCGPoint: location]];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.delegate
        &&
        [self.delegate respondsToSelector: @selector(didShoot:)]
        ) {
        UITouch* touch = [touches anyObject];
        CGPoint location = [touch locationInView: self];
        [self.delegate didShoot: [NSValue valueWithCGPoint: location]];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.delegate
        &&
        [self.delegate respondsToSelector: @selector(didShoot:)]
        ) {
        UITouch* touch = [touches anyObject];
        CGPoint location = [touch locationInView: self];
        [self.delegate didShoot: [NSValue valueWithCGPoint: location]];
    }
}

@end
