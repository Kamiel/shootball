//
//  BlinkView.h
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TexturedView.h"

@interface BlinkView : TexturedView
@property (nonatomic, assign, readwrite) BOOL accentuated;
- (void)hit;
@end
