//
//  BallView.m
//  shootball
//
//  Created by Pitometsu on 9/4/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import "BallView.h"
#import "TexturedView+Protected.h"


@implementation BallView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.image.image = [UIImage imageNamed: @"ball.png"];
    }
    return self;
}

@end
