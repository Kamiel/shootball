//
//  TexturedView.m
//  shootball
//
//  Created by Pitometsu on 9/5/12.
//  Copyright (c) 2012 Pitometsu. All rights reserved.
//

#import "TexturedView.h"
#import "TexturedView+Protected.h"


@implementation TexturedView

@synthesize image = _image;

- (void)dealloc {
    self.image = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = NO;
        self.image = [[[UIImageView alloc] initWithFrame: self.bounds]
                      autorelease];
        self.image.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview: self.image];
    }
    return self;
}

@end
